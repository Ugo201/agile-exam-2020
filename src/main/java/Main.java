import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;



public class Main 
{
	 
    public static void main( String[] args ) throws IOException, SQLException
    {

    	BufferedReader reader =new BufferedReader(new InputStreamReader(System.in)); 
    	
    	int option;
    	
    	if(args.length ==0) {   
    		option= 1;
    	}else {
    		option=7;			// used for jenkins webhook
    	}
				
       
   	 	SQL sq = new SQL(); 
        sq.connection(); 			
        
        
    	//	Meeting m =  new SqlQuery();  -- Liskov substitution principle without singleton design pattern
        
   	 	Meeting m =  SQL.GetMeetingInstance(); // Liskov substitution principle with singleton design pattern

        
        while(option!=7) {
        	menu();		
            option = Integer.parseInt(reader.readLine());
            switch(option) { 				// factory pattern with open/close principle 
            case 1:
            	prompt(m); 		
            	 sq = new CreateMeeting(sq);  // Decorator pattern 	
            	 sq.query(m);  				// with Open/Close principle with; Create a meeting slot
            	 break;
            case 2:
            	sq = new ReadAllMeeting(sq);	//  Decorator pattern 
            	sq.query();             	// with Open/Close principle; Read all meeting
	           	 break;
            case 3:
            	promptUpdate(m); 				
            	sq = new UpdateMeeting(sq);  	// Decorator pattern   
            	sq.query(m);				// with Open/Close principle; Update Meeting slot
	           	 break;   	 
            case 4:
	           	 System.out.println(":: Enter ID to delete ::");
	           	 m.setID(Integer.parseInt(reader.readLine()));		
	           	 sq = new DeleteMeeting(sq);		// Decorator pattern   
	           	 sq.query(m);				 	// with Open/Close principle; Delete a meeting slot
	           	 break;
            case 5:
	           	 System.out.println(" :: Enter date to filter meeting slots ::");
	           	 m.setStartDate(reader.readLine());
	           	 sq = new GetMeetingByTimeFrame(sq); 	// Decorator pattern 
	           	 sq.query(m);				// with Open/Close principle; Get meeting(s) by time frame
	           	 break;
            case 6:
            	System.out.println(" :: Enter meeting ID to search ::");
            	m.setID(Integer.parseInt(reader.readLine()));
            	sq= new SearchMeeting(sq);			// Decorator pattern 
	           	sq.query(m);						// with Open/Close principle; Search meeting slot
	           	 break;
            case 7:
	           	 break;
	        default:
	        	 System.out.println("Unknown option");
	        	 break;
            }
        }
    }
    
    
    
    
    
    
    public static void menu() {
        System.out.println( ":: MEETING RESERVATION SYSTEM ::");
        System.out.println("Enter 1 to insert meeting slot");
        System.out.println("Enter 2 to view all meeting slots");
        System.out.println("Enter 3 to update meeting slot");
        System.out.println("Enter 4 to delete a meeting slot");
        System.out.println("Enter 5 to filter meeting slot by date");
        System.out.println("Enter 6 to search meeting slot");
        System.out.println("Enter 7 to Exit");
    }
    
   public static void prompt(Meeting m) throws IOException {
	   
   	BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
  	 
   	System.out.println(":: Enter Details ::");
  	
   	System.out.println("Enter Topic:");
  	 m.setTopic(reader.readLine());
  	
  	 System.out.println("Enter Host:");
  	 m.setHost(reader.readLine());
  	 
  	 System.out.println("Enter Start Date:");
  	 m.setStartDate(reader.readLine());
  	 
  	 System.out.println("Enter End Date:");
  	 m.setEndDate(reader.readLine());
  	 
  	 System.out.println("Enter Room:");
  	 m.setRoom(reader.readLine());
  	 
  	 System.out.println("Enter User Count:");
  	 m.setUserCount(Integer.parseInt(reader.readLine()));
	   
   }
   
   public static void promptUpdate(Meeting m) throws IOException {
	   
	   	BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
	  	 
	   	System.out.println(":: Enter Details ::");
	   	
	   	System.out.println("Enter Id:");
	  	 m.setID(Integer.parseInt(reader.readLine()));
	  	
	   	System.out.println("Enter Topic:");
	  	 m.setTopic(reader.readLine());
	  	
	  	 System.out.println("Enter Host:");
	  	 m.setHost(reader.readLine());
	  	 
	  	 System.out.println("Enter Start Date:");
	  	 m.setStartDate(reader.readLine());
	  	 
	  	 System.out.println("Enter End Date:");
	  	 m.setEndDate(reader.readLine());
	  	 
	  	 System.out.println("Enter Room:");
	  	 m.setRoom(reader.readLine());
	  	 
	  	 System.out.println("Enter User Count:");
	  	 m.setUserCount(Integer.parseInt(reader.readLine()));
		   
	   }
	   
}

