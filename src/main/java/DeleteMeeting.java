import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteMeeting extends Decorator {

	public DeleteMeeting(SQL sq) {
		super(sq);
		// TODO Auto-generated constructor stub
	}

	@Override
    public boolean query(Meeting m)  {
    	
    	SQL sqlQ = new SQL();
    	Connection conn = sqlQ.connection();
    	
    	int rowsDeleted=0;
		
    	try {
	    	
	    	String sql = "DELETE FROM meeting WHERE ID=?"; 
	    	PreparedStatement statement;
			statement = conn.prepareStatement(sql);
	    	statement.setInt(1, m.getID());
	    	 
	    	rowsDeleted = statement.executeUpdate();
	    	
	    	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (rowsDeleted > 0) {
	    	   return true;
	    	}
		
		return false;

    }
}
