import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GetMeetingByTimeFrame  extends Decorator {

	public GetMeetingByTimeFrame(SQL sq) {
		super(sq);
		// TODO Auto-generated constructor stub
	}

	@Override
    public boolean query(Meeting m){
    	
    	SQL sqlQ = new SQL();
    	Connection conn = sqlQ.connection();
    	
    	int count=0;

		try {
	    	String sql = "SELECT * FROM meeting where DATE(startdate) >= '"+m.getStartDate()+"'";
	    	 
	    	Statement statement;
	    	
			statement = conn.createStatement();
			
			ResultSet result =  statement.executeQuery(sql);
	    	 
   		 while(result.next()) {
   	    		int id = result.getInt(1);
   	    	    String topic = result.getString(2);
   	    	    String host = result.getString(3);
   	    	    String startDate = result.getString(4);
   	    	    String endDate = result.getString(5);
   	    	    String room = result.getString(6);
   	    	    int userCount = result.getInt(7);
   	    	    count++;
   	    	    String output = "Meetind #%d: %s | %s | %s | %s | %s | %d";
   	    	    System.out.println(String.format(output, id, topic, host, startDate, endDate,room,userCount));
   	    	}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    
    		
		 if(count>0) {
			 return true;
		 }
			 return false;

    }
}
