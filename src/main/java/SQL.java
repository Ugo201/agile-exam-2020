import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQL {
	
	Connection conn;
    public static Meeting instance = null;
    
	 public  Connection connection() {
    	String dbURL = "jdbc:mysql://localhost:3306/meeting_app";
    	String username = "root";
    	String password = "";
    	try {
    		
    	     conn = DriverManager.getConnection(dbURL, username, password);
    	 
    	    if (conn != null) {
    	        return conn;
    	    }
    	} catch (SQLException ex) {
    	    ex.printStackTrace();
    	}
		return null;
    }

	 public boolean query() {
		 
		 return false;
	 }
	 
	 public boolean query(Meeting m) {
		 return false;
	 }
	 
	 
	 public static Meeting GetMeetingInstance() {
		 if(instance == null) {
			 instance = new Meeting();
		 }
		 return instance;
	 }
	 
	 
}
