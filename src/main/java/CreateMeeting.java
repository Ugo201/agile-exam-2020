import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateMeeting extends Decorator {

	public CreateMeeting(SQL sq) {
		super(sq);
	// TODO Auto-generated constructor stub
	}

	@Override
    public  boolean query(Meeting m) {
    	
    	SQL sqlQ = new SQL();
    	Connection conn = sqlQ.connection();
    	
    	int rowsInserted = 0;
    	String sql = "INSERT INTO meeting (ID,topic, host, startdate,enddate, room, usercount) VALUES (?, ?, ?, ?, ?, ?, ?)";
    	 
    	PreparedStatement statement;
		try {
			statement = conn.prepareStatement(sql);
			statement.setString(1, null);
	    	statement.setString(2, m.getTopic());
	    	statement.setString(3, m.getHost());
	    	statement.setString(4, m.getStartDate());
	    	statement.setString(5, m.getEndDate());
	    	statement.setString(6, m.getRoom());
	    	statement.setInt(7, m.getUserCount());
	    	 
	    	rowsInserted = statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	if (rowsInserted > 0) {
    	    return true;
    	}
		return false;

    }
}
