import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateMeeting extends Decorator{
	
	public UpdateMeeting(SQL sq) {
		super(sq);
		// TODO Auto-generated constructor stub
	}

	@Override
    public  boolean query(Meeting m) {
		
    	SQL sqlQ = new SQL();
    	Connection conn = sqlQ.connection();
    	
    	int rowsUpdated=0;
    	
    	try {
        	String sql = "UPDATE meeting SET topic=?, host=?, startdate=?, enddate=?, room=?, usercount=? where ID=? ";
        	PreparedStatement statement = conn.prepareStatement(sql);
			statement.setString(1, m.getTopic());
	    	statement.setString(2, m.getHost());
	    	statement.setString(3, m.getStartDate());
	    	statement.setString(4, m.getEndDate());
	    	statement.setString(5, m.getRoom());
	    	statement.setInt(6, m.getUserCount());
	    	statement.setInt(7, m.getID());
	    	
	    	rowsUpdated = statement.executeUpdate();
	    	
	    	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	if (rowsUpdated > 0) {
    	    return true;
    	}
    	
		return false;
    	
    }
}
