import static org.junit.Assert.assertEquals;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;

public class AgileExamUnitTest {

	SQL sqlQ ;
	Connection conn;
	Meeting m;
@Before
public void setUp() {
	
	sqlQ = new SQL();
	conn = sqlQ.connection();
}


@Test
public void TestCreateQuery() {
	
	CreateMeeting cm = new CreateMeeting(sqlQ);
	
	m = new Meeting();
	
	m.setTopic("test topic");
	m.setHost("test host");
	m.setStartDate("2013-02-11");
	m.setEndDate("2013-02-11");
	m.setRoom("test room");
	m.setUserCount(200);
	
	assertEquals(true, cm.query(m));
}

@Test
public void TestReadAllMeetingQuery() {
	
	ReadAllMeeting rm= new ReadAllMeeting(sqlQ);
	
	assertEquals(true, rm.query());
}

@Test
public void TestUpdateQuery() {
	
	UpdateMeeting um = new UpdateMeeting(sqlQ);
	
	m = new Meeting();
	
	m.setID(16);
	m.setTopic("update topic");
	m.setHost("update host");
	m.setStartDate("2013-02-11");
	m.setEndDate("2013-02-11");
	m.setRoom("update room");
	m.setUserCount(400);
	
	assertEquals(true, um.query(m));
}

@Test
public void TestDeleteQuery() {
	
	DeleteMeeting dm = new DeleteMeeting(sqlQ);
	
	m = new Meeting();
	
	m.setID(29);
	
	assertEquals(true, dm.query(m));
	
}

@Test
public void TestFilterMeetingSlot() {
	
	GetMeetingByTimeFrame gmt = new GetMeetingByTimeFrame(sqlQ);
	
	m = new Meeting();
	
	m.setStartDate("2013-02-11");
	
	assertEquals(true, gmt.query(m));
}

@Test
public void TestMeetingExistence() {
	
	SearchMeeting sm = new SearchMeeting(sqlQ);
	
	m = new Meeting();
	
	m.setID(2);
	
	assertEquals(true, sm.query(m));
	
	
}
}
