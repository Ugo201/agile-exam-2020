import static org.junit.Assert.assertEquals;

import java.sql.Connection;

import org.junit.Test;

public class AgileExamIntegrationTest {

@Test
public void TestCreateQuery() {
	
	SQL  sqlQ = new SQL();
	Connection  conn = sqlQ.connection();
	
	CreateMeeting cm = new CreateMeeting(sqlQ);
	
	Meeting m = new Meeting();
	
	m.setTopic("test topic");
	m.setHost("test host");
	m.setStartDate("2013-02-11");
	m.setEndDate("2013-02-11");
	m.setRoom("test room");
	m.setUserCount(200);
	
	assertEquals(true, cm.query(m));
}

@Test
public void TestReadAllMeetingQuery() {
	
	SQL  sqlQ = new SQL();
	
	Connection  conn = sqlQ.connection();
	
	ReadAllMeeting rm= new ReadAllMeeting(sqlQ);
	
	assertEquals(true, rm.query());
}

@Test
public void TestUpdateQuery() {

	SQL  sqlQ = new SQL();
	Connection  conn = sqlQ.connection();
	
	UpdateMeeting um = new UpdateMeeting(sqlQ);
	
	Meeting m = new Meeting();
	
	m.setID(16);
	m.setTopic("update topic");
	m.setHost("update host");
	m.setStartDate("2013-02-11");
	m.setEndDate("2013-02-11");
	m.setRoom("update room");
	m.setUserCount(400);
	
	assertEquals(true, um.query(m));
}

@Test
public void TestDeleteQuery() {
	
	SQL  sqlQ = new SQL();
	Connection  conn = sqlQ.connection();
	
	DeleteMeeting dm = new DeleteMeeting(sqlQ);
	
	Meeting m = new Meeting();
	
	m.setID(31);
	
	assertEquals(true, dm.query(m));
	
}

@Test
public void TestFilterMeetingSlot() {
	
	SQL  sqlQ = new SQL();
	Connection  conn = sqlQ.connection();
	
	GetMeetingByTimeFrame gmt = new GetMeetingByTimeFrame(sqlQ);
	
	Meeting m = new Meeting();
	
	m.setStartDate("2013-02-11");
	
	assertEquals(true, gmt.query(m));
}

@Test
public void TestMeetingExistence() {
	
	SQL  sqlQ = new SQL();
	Connection  conn = sqlQ.connection();
	
	SearchMeeting sm = new SearchMeeting(sqlQ);
	
	Meeting m = new Meeting();
	
	m.setID(2);
	
	assertEquals(true, sm.query(m));
	
	
}
}
